-- SUMMARY --

The content type image module provides the ability to upload an image to each
content type that gets displayed next to the content type on the node add screen.
Content type image uses imagecache to resize the uploaded image to 150x110.

For a full description of the module, visit the project page:
  http://drupal.org/project/content_type_image // To be updated when approved.

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/content_type_image // To be updated when approved.


-- REQUIREMENTS --

Content type image requires imagecache, imageapi and one of the imageapi toolkits.


-- INSTALLATION --

* Install as usual.  Make sure to enable one of the ImageApi toolkits.


-- CONFIGURATION --

* Browse to the content type edit page and upload an image in the 'Screenshot Settings'
fieldset.

-- CUSTOMIZATION --

* To override the default imagecache size:

  1) Enable the imagecache UI on the modules page

  2) Navigate to: /admin/build/imagecache/list
  
  3) Click on 150x110_content_type_image and click the 'Override Defaults' button
  
  4) Adjust the css to accomodate your new image sizes.

    - Copy the css from content_type_image.css and add it to your css file and override.
